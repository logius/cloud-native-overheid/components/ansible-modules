#!/usr/bin/python
#
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

ANSIBLE_METADATA = {'metadata_version': '0.2',
                    'status': ['preview'],
                    'supported_by': 'Team Standaard Platform'}

DOCUMENTATION = '''
---
module: k8s_copy_secret
short_description: Copy K8s secret
version_added: "2.9"
author: "Team Standaard Platform"
description:
   - Copy k8s secret into another namespace, optionally with a new name.
   - Existing secrets will be updated.
   - Changed flag reports if work has been done.
options:
  api_version:
    description:
      - Version of the API.
    default: v1
  name:
    description:
      - Name of the secret.
    required: True
  namespace:
    description:
      - Namespace where the secret can be found.
    required: True
  target_name:
    description:
      - New name of the secret (optional).
  target_namespace:
    description:
      - Namespace where the secret should be created.
    required: True
'''

EXAMPLES = '''
- name: Copy Secret
  k8s_copy_secret:
    name: wildcard-certificate
    namespace: my_namespace
    target_namespace: my_new_secret

- name: Copy Secret with a new name
  k8s_copy_secret:
    name: wildcard-certificate
    namespace: certs_namespace
    target_namespace: my_namespace
    target_name: my_new_secret
'''


from ansible_collections.kubernetes.core.plugins.module_utils.ansiblemodule import (
    AnsibleModule,
)

import base64

def execute_module(module, k8s_ansible_mixin):
    result = {}
    secrets = k8s_ansible_mixin.kubernetes_facts(
        module.params["kind"],
        module.params["api_version"],
        name=module.params["name"],
        namespace=module.params["namespace"],
        label_selectors=module.params["label_selectors"],
        field_selectors=module.params["field_selectors"],
    )

    resource = k8s_ansible_mixin.find_resource('Secret', module.params['api_version'])

    if len(secrets['resources']) == 1:
        secret = secrets['resources'][0]
        metadata = secret['metadata']

        existing = k8s_ansible_mixin.kubernetes_facts(secret['kind'],
                                  secret['apiVersion'],
                                  metadata['name'],
                                  module.params['target_namespace'], [], [])

        if module.params['target_name']:
            metadata['name'] = module.params['target_name']
        metadata['namespace'] = module.params['target_namespace']
        metadata['resourceVersion'] = metadata['uid'] = ''

        k8s_obj = resource.apply(secret).to_dict()
        result['secret'] = k8s_obj

        if len(existing['resources']) > 0:
            existing_secret = existing['resources'][0]
            match, _ = k8s_ansible_mixin.diff_objects(existing_secret, k8s_obj)
            result['changed'] = not match
        else:
            result['changed'] = True

    elif len(secrets['resources']) == 0:
        module.fail_json(msg="Could not find secret")
    else:
        module.fail_json(msg="Found more than one secret")

    module.exit_json(**result)

def argspec():
    return dict(
            kind=dict(default="Secret"),
            api_version=dict(default="v1", aliases=["api", "version"]),
            name=dict(),
            namespace=dict(),
            target_name=dict(default=None),
            target_namespace=dict(required=True),
            label_selectors=dict(type="list", elements="str", default=[]),
            field_selectors=dict(type="list", elements="str", default=[]),
        )
    
def main():
    module = AnsibleModule(argument_spec=argspec(), supports_check_mode=True)
    from ansible_collections.kubernetes.core.plugins.module_utils.common import (
        K8sAnsibleMixin,
        get_api_client,
    )

    k8s_ansible_mixin = K8sAnsibleMixin(module)
    k8s_ansible_mixin.client = get_api_client(module=module)
    k8s_ansible_mixin.fail_json = module.fail_json
    k8s_ansible_mixin.fail = module.fail_json
    k8s_ansible_mixin.exit_json = module.exit_json
    k8s_ansible_mixin.warn = module.warn
    execute_module(module, k8s_ansible_mixin)


if __name__ == "__main__":
    main()
