#!/usr/bin/python
#
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

ANSIBLE_METADATA = {'metadata_version': '0.2',
                    'status': ['preview'],
                    'supported_by': 'Team Standaard Platform'}

DOCUMENTATION = '''
---
module: k8s_secret_info
short_description: Get decoded values from K8s secret.
version_added: "2.9"
author: "Team Standaard Platform"
description:
   - Get decoded values from K8s secret.
   - Empty results are returned if secret could not be found.
options:
  name:
    description:
      - Name of the secret.
    required: True
  namespace:
    description:
      - Namespace where the secret can be found.
    required: True
'''

EXAMPLES = '''
- name: Get secret info
  secret_info:
    name: keycloak-http
    namespace: my_namespace
  register: keycloak_password
'''

RETURN = r''' # '''

from ansible_collections.kubernetes.core.plugins.module_utils.ansiblemodule import (
    AnsibleModule,
)

import base64

def execute_module(module, k8s_ansible_mixin):
    secrets = k8s_ansible_mixin.kubernetes_facts(
        module.params["kind"],
        module.params["api_version"],
        name=module.params["name"],
        namespace=module.params["namespace"],
        label_selectors=module.params["label_selectors"],
        field_selectors=module.params["field_selectors"],
    )

    result = {}
    result["num_secrets"] = len(secrets['resources'])
    if len(secrets['resources']) > 0:
      secret = secrets['resources'][0]
      for name, value in secret['data'].items():
        result[name] = base64.b64decode(value)

    module.exit_json(changed=False, **result)

def argspec():
    return dict(
            kind=dict(default="Secret"),
            api_version=dict(default="v1", aliases=["api", "version"]),
            name=dict(),
            namespace=dict(),
            label_selectors=dict(type="list", elements="str", default=[]),
            field_selectors=dict(type="list", elements="str", default=[]),
        )
    
def main():
    module = AnsibleModule(argument_spec=argspec(), supports_check_mode=True)
    from ansible_collections.kubernetes.core.plugins.module_utils.common import (
        K8sAnsibleMixin,
        get_api_client,
    )

    k8s_ansible_mixin = K8sAnsibleMixin(module)
    k8s_ansible_mixin.client = get_api_client(module=module)
    k8s_ansible_mixin.fail_json = module.fail_json
    k8s_ansible_mixin.fail = module.fail_json
    k8s_ansible_mixin.exit_json = module.exit_json
    k8s_ansible_mixin.warn = module.warn
    execute_module(module, k8s_ansible_mixin)


if __name__ == "__main__":
    main()