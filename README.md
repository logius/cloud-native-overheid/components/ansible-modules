# ansible-modules

The project contains some Ansible modules for CNO tasklists. 

The modules are compatible with the kubernetes.core version 2.2 (see https://docs.ansible.com/ansible/latest/collections/kubernetes/core/)

| module              | description                                   |
|:------------------- |:--------------------------------------------- |
| helm3               | Install helm applications                     |
| k8s_copy_secret     | Copy k8s secret between namespaces            |
| k8s_secret_info     | Read and decode secret                        |
| skopeo_copy         | Copy an image between registries using skopeo |
