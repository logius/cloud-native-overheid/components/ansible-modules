#!/usr/bin/python
#
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

import requests
import re
from ansible.module_utils.basic import AnsibleModule
ANSIBLE_METADATA = {'metadata_version': '0.1',
                    'status': ['preview'],
                    'supported_by': 'Team CNO'}

DOCUMENTATION = '''
---
module: gitlab_access_token_v2
short_description: Get GitLab access token
version_added: "2.9"
author: "Team CNO"
description:
   - Login with username/password in the GitLab user interface.
   - Retrieve a new personal access token for the logged in user.
   - Fail when user cannot login or access token could not be retrieved.
   - v2 of the module is for use in gitlab role only.
options:
  url:
    description:
      - GitLab url, including protocol e.g. https://www.gitlab.com
    required: True
  username:
    description:
      - Name of user
    required: True
  password:
    description:
      - Password of user
    required: True
'''

EXAMPLES = '''
- name: Get API access token
  gitlab_access_token_v2:
    url: https://www.gitlab.com
    username: mylogin
    password: mysecret
'''

RETURN = r''' # '''


class GitLabAccessToken(AnsibleModule):

    def __init__(self, *args, **kwargs):
        AnsibleModule.__init__(self, self.argspec, supports_check_mode=True)

    def get_authenticity_token(self, session, url):
        '''Get form page to retrieve the authentication token for CSRF'''
        content = session.get(url).text
        for line in content.split('\n'):
            m = re.search('name="authenticity_token" value="([^"]+)"', line)
            if m:
                return m.group(1)

        self.fail_json(msg='Unable to find the authenticity token for ' + url)

    def login(self, session):
        '''Login to GitLab, cookies are stored in session object'''
        gitlab_url = self.params['url']
        sign_in_url = gitlab_url + '/users/sign_in?auto_sign_in=false'

        authenticity_token = self.get_authenticity_token(session, sign_in_url)

        body = {
            'user[login]': self.params['username'],
            'user[password]': self.params['password'],
            'authenticity_token': authenticity_token
        }
        r = session.post(sign_in_url, data=body)
        if r.status_code != 200 or 'Invalid Login' in r.text:
            self.fail_json(
                msg='Failed to log in to GitLab with user ' + self.params['username'])

    def get_personal_access_token(self, session):
        '''Get personal access token for user logged in with session object'''
        from datetime import datetime
        from datetime import timedelta

        access_tokens_url = self.params['url'] + \
            '/-/profile/personal_access_tokens'
        authenticity_token = self.get_authenticity_token(
            session, access_tokens_url)
        access_token_name = 'api-token-' + datetime.now().strftime('%Y%m%d%H%M')
        expires_at = datetime.now() + timedelta(days=1)

        body = {
            'personal_access_token[name]': access_token_name,
            'personal_access_token[scopes][]': 'api',
            'personal_access_token[expires_at]': expires_at,
            'authenticity_token': authenticity_token
        }

        response = session.post(access_tokens_url, data=body)

        if response.ok:
            for line in response.text.split('\n'):
                m = re.search(
                    'id="created-personal-access-token" value="([^"]+)"', line)
                if m:
                    return m.group(1)

        self.exit_json(msg='Unable to get access token')

    def get_api_token(self):
        result = {}

        session = requests.Session()

        self.login(session)

        result['access_token'] = self.get_personal_access_token(session)

        self.exit_json(**result)

    @property
    def argspec(self):
        args = {}
        args.update(
            dict(
                url=dict(required=True),
                username=dict(required=True),
                password=dict(required=True)
            )
        )
        return args


if __name__ == '__main__':
    GitLabAccessToken().get_api_token()
