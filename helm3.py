#!/usr/bin/python
#
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

ANSIBLE_METADATA = {'metadata_version': '0.1',
                    'status': ['preview'],
                    'supported_by': 'Team Standaard Platform'}

DOCUMENTATION = '''
---
module: helm3
short_description: Manages Kubernetes packages with the Helm 3 package manager
version_added: "2.9"
author: "Team Standaard Platform"
description:
   - Install, upgrade and delete packages with the Helm 3 package manager.
   - The Helm operations are executed with a system call. There are no dependencies on pyhelm.
options:
  atomic:
    description:
      - If set, upgrade process rolls back changes made in case of failed upgrade.
    default: false
  chart:
    description:
      - A map describing the chart to install. See examples for available options.
    default: {}
  create_namespace:
    description:
      - If set, namespace is created.
    default: false
  debug:
    description:
      - Run helm in debug mode.
    default: False
  extrArgs:
    description:
      - A list providing extra command line args.
    default: []
  files:
    description:
      - A list of YAML files or URLs providing values for the chart.
    default: []
  force:
    description:
      - Run helm with the --force option.
    default: False
  namespace:
    description:
      - Kubernetes namespace where the chart should be installed.
    default: "default"
    required: True
  patches:
    description:
      - List of patches to apply before the chart is deployed.
      - Each patch must correspond with a manifest, identified by apiVersion, kind and metadata.name.
    default: []
    required: False
  release:
    description:
      - Release name to manage.
    required: True
  state:
    description:
      - Whether to install C(present) or remove C(absent) a package.
    choices: ['absent', 'present']
    default: "present"
  values:
    description:
      - YAML with values for the chart.
    default: {}
  wait:
    description:
      - If set, will wait until all objects are in a ready state.
'''

EXAMPLES = '''
- name: Install helm chart
  helm3:
    state: present
    release: my-nginx
    namespace: default
    chart:
      name: stable/nginx
      version: 1.2.3

- name: Install helm chart from repo
  helm3:
    state: present
    release: harbor
    namespace: default
    chart:
      name: harbor
      version: v1.3.0
      repo: https://helm.goharbor.io
    wait: true

- name: Install helm chart with values
  helm3:
    chart:
      name: stable/chaoskube
      version: 1.2.3
    state: present
    release: chaoskube
    namespace: chaoskube
    values:
      dryRun: false
      labels: chaos=true

- name: Install helm chart with value files
  helm3:
    chart:
      name: stable/chaoskube
      version: 1.2.3
    state: present
    release: chaoskube
    namespace: chaoskube
    files:
      - my-values.yaml

- name: Install with patches
  helm3:
    chart:
      name: stable/mattermost
      version: 1.2.3
    state: present
    release: mattermost
    patches:
      - apiVersion: apps/v1
        kind: Deployment
        metadata:
          name: mattermost-team-edition
        spec:
          template:
            spec:
              securityContext:
                runAsUser: 2000

- name: Uninstall helm chart
  helm3:
    state: absent
    release: my-nginx
    namespace: default

'''

RETURN = r''' # '''

from ansible.module_utils.basic import AnsibleModule
import ansible.module_utils.basic
import tempfile
import yaml

def has_helm3_release(module, release, namespace):
    args = ['helm', 'history', release, '--namespace', namespace]
    rc, out, err = module.run_command(args)

    return rc == 0

def build_install_args(module, release, namespace):
    params = module.params
    chart = params.get('chart')
    chartname = chart.get('name')

    args = ['helm', 'upgrade', '--install', release, chartname, '--namespace', namespace ]

    chartversion = chart.get('version')
    if chartversion:
        args.append('--version')
        args.append(chartversion)

    chartrepo = chart.get('repo')
    if chartrepo:
        args.append('--repo')
        args.append(chartrepo)

    if (params.get('atomic')):
        args.append('--atomic')

    if (params.get('create_namespace')):
        args.append('--create-namespace')

    if (params.get('debug')):
        args.append('--debug')

    if (params.get('force')):
        args.append('--force')

    if (params.get('timeout')):
        args.append('--timeout')
        timeout = params.get('timeout')
        if timeout.isdigit():
          timeout += 's'
        args.append(timeout)

    if (params.get('wait')):
        args.append('--wait')

    if (params.get('extraArgs')):
        for arg in params.get('extraArgs'):
            args.append(arg)

    if params.get('files'):
        for file in params.get('files'):
            args.append("-f")
            args.append(file)

    return args

# inspired by https://docs.solo.io/gloo/latest/installation/gateway/kubernetes/helm_advanced/
def apply_patches(args, patches):
    import os
    import stat

    tmpdir = tempfile.mkdtemp(prefix="")

    patchfiles = []

    # write patches to separate files.
    for patch in patches:

        patchfile = "patch{0}.yaml".format(len(patchfiles) + 1)
        with open(os.path.join(tmpdir, patchfile), 'w') as outfile:
            yaml.dump(patch, outfile, default_flow_style=False)
        patchfiles.append(patchfile)

    # kustomization.yaml with resources and patch files for use by kustomize.
    with open(os.path.join(tmpdir, 'kustomization.yaml'), 'w') as writer:

        # output from helm will be put into manifests.yaml.
        writer.write("resources:\n")
        writer.write("- manifests.yaml\n")

        # patch files for merging
        writer.write("patchesStrategicMerge:\n")
        for patchfile in patchfiles:
            writer.write("- {0}\n".format(patchfile))

    # kustomize shell script for use as post-renderer in helm.
    kustomize_script = os.path.join(tmpdir, 'kustomize.sh')
    with open(kustomize_script, 'w') as writer:
        writer.write("#!/bin/sh\n")
        writer.write("cat > {0}/manifests.yaml\n".format(tmpdir))
        writer.write("exec kubectl kustomize {0}\n".format(tmpdir))

    os.chmod(kustomize_script, stat.S_IREAD | stat.S_IWRITE | stat.S_IEXEC)

    # add kustomize script as post-renderer for helm.
    args.append("--post-renderer")
    args.append(kustomize_script)

def install(module):
    """Install Helm Release."""
    params = module.params

    if not params.get('chart') or not params.get('chart').get('name'):
        module.fail_json(msg='Missing required field chart.name')

    release = params['release']
    namespace = params['namespace']

    args = build_install_args(module, release, namespace)

    if params.get('patches'):
        apply_patches(args, params.get('patches'))

    values_file = tempfile.NamedTemporaryFile()

    try:
        values = params.get('values')
        if values:
            with open(values_file.name, 'w') as outfile:
                yaml.dump(values, outfile, default_flow_style=False)
            args.append("-f")
            args.append(values_file.name)

        rc, out, err = module.run_command(args, check_rc=True)

    finally:
        values_file.close()

    return dict(cmd=' '.join(args), stdout=out, stderr=err, changed=True)

def delete(module):
    """Remove helm release."""
    params = module.params

    release = params['release']
    namespace = params['namespace']

    if has_helm3_release(module, release, namespace):
        args = ['helm', 'uninstall', release, '--namespace', namespace]
    else:
        return dict(changed=False)

    rc, out, err = module.run_command(args, check_rc=True)

    return dict(cmd=args, stdout=out, stderr=err, changed=True)

def main():
    """The main function."""
    module = AnsibleModule(
        argument_spec=dict(
            atomic=dict(type='bool', default=False),
            chart=dict(type='dict'),
            create_namespace=dict(type='bool', default=False),
            debug=dict(type='bool', default=False),
            extraArgs=dict(type='list'),
            files=dict(type='list'),
            force=dict(type='bool', default=False),
            namespace=dict(type='str', default='default'),
            release=dict(type='str', required=True),
            timeout=dict(type='str'),
            state=dict(choices=['absent', 'present'], default='present'),
            values=dict(type='dict'),
            patches=dict(type='list'),
            wait=dict(type='bool', default=False)
        ),
        supports_check_mode=True)

    params = module.params

    state = module.params['state']

    if state == 'present':
        rst = install(module)
    elif state == 'absent':
        rst = delete(module)

    module.exit_json(**rst)


if __name__ == '__main__':
    main()
