#!/usr/bin/python
#
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

ANSIBLE_METADATA = {'metadata_version': '0.1',
                    'status': ['preview'],
                    'supported_by': 'Team Standaard Platform'}

DOCUMENTATION = '''
---
module: skopeo_copy
short_description: Perform skopeo copy
version_added: "2.9"
author: "Team Standaard Platform"
description:
   - Copy an image (manifest, filesystem layers, signatures) from one location to another.
   - Ansible wrapper around skopeo: https://github.com/containers/skopeo/blob/master/docs/skopeo-copy.1.md
options:
  source_image:
     description:
      - reference to source image (docker, oci, dir)
      - see examples in skopeo documentation
    required: True
  src_creds:
    description:
      - Credentials for logging into source registry
  destination_image:
     description:
      - reference to target image (docker, oci, dir)
    required: True
  dest_creds:
    description:
      - Credentials for logging into destination registry
'''

EXAMPLES = '''
- name: Copy busy box
  skopeo_copy:
    source_image: docker://busybox:latest
    destination_image: docker://my-private-registry:my-images/busybox:latest
'''

RETURN = r''' # '''

from ansible.module_utils.basic import AnsibleModule
import ansible.module_utils.basic

def skopeo_copy(module):
    source_image = module.params['source_image']
    destination_image = module.params['destination_image']
    src_creds = module.params.get('src_creds')
    dest_creds = module.params.get('dest_creds')

    args = ['skopeo', 'copy', source_image, destination_image, '--format=v2s2' ]
    if src_creds:
      args.extend( [ '--src-creds', src_creds ] )
    if dest_creds:
      args.extend( [ '--dest-creds', dest_creds ] )

    rc, out, err = module.run_command(args, check_rc=True)

    return dict(stdout=out, stderr=err)

def main():
    """The main function."""
    module = AnsibleModule(
        argument_spec=dict(
            source_image =dict(type='str', required=True),
            destination_image=dict(type='str', required=True),
            src_creds=dict(type='str', required=False),
            dest_creds=dict(type='str', required=False)
        ),
        supports_check_mode=True)

    rst = skopeo_copy(module)

    module.exit_json(**rst)

if __name__ == '__main__':
    main()
